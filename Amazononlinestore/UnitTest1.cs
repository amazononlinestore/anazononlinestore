using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Threading;
using AmazonOnlineStores.BaseClass;
using AmazonOnlineStores.PageObjects;
using System;
using AmazonOnlineStores.TestData;


namespace AmazonOnlineStores
{
    [TestFixture]
    public class Tests : BaseTest
    {
              
        [Test]
        //Verify if Amazon Website Loaded Properly
        public void VerifyAmazonWebsiteLoad()
        {
            HomePage hp = new HomePage(driver);
           
            Assert.AreEqual("Amazon", hp.LoadHomescreen());
           
            }

        [Test]
        //Verify user able to search a product b it product name and to check the availability 
        public void VerifySearchProdctWithName()
        {
           var UserData = DataAccess.GetTestData("TC001");

           string productName = UserData.productName;

            SearchPage sp= new SearchPage(driver);

            sp.searchWithProductName(productName);

            ResultPage resultSscreen = new ResultPage(driver);

                       
            Assert.AreEqual(UserData.avaialability, resultSscreen.IsProductAvaialble(productName));



        }
        [Test]
        //Verify user able to add to cart for the product selected
        public void VerifySearchedProductAddedToCart()
         {
           var UserData = DataAccess.GetTestData("TC001");

            string productName = UserData.productName;
            SearchPage sp = new SearchPage(driver);
            ResultPage resultSscreen = new ResultPage(driver);
            sp.searchWithProductName(productName);
            ProductPage product = new ProductPage(driver);
            string productsearched = product.NavigateToProductscreen(productName);
            string Addedsuccess= product.AddToCart(productName);                        
            Assert.AreEqual("Added to Cart", Addedsuccess);
            
        }
        [Test]
        //Verify Cart Pageloaded successfully
        public void VerifyEmptyCartPageLoad()
        {
            HomePage hp = new HomePage(driver);
            hp.NavigateToCart();
            AddToCartPage emptycart = new AddToCartPage(driver);
            string emptycartpage = emptycart.ValideEmptyCart();
            Assert.AreEqual("Your Amazon Basket is empty", emptycartpage);
           
        }
      
    }
}