using Dapper;
using System.Configuration;
using System.Data.OleDb;
using System.Linq;

namespace AmazonOnlineStores.TestData
{
    public class DataAccess
    {
        public static string TestDataFileConnection()
        {
            var path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = path.Substring(6) + @"\TestData\TestData.xlsx";
            var con = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source ={0}; Extended Properties=Excel 12.0;", path);
            return con;
           

        }
        public static UserData GetTestData(string keyName)
        {
            using (var connection = new OleDbConnection(TestDataFileConnection()))
            {
                connection.Open();
                var query = string.Format("select * from [DataSet$] where testID='{0}'", keyName);
                var value = connection.Query<UserData>(query).FirstOrDefault();
                connection.Close();
                return value;
            }
        }

    }
}
