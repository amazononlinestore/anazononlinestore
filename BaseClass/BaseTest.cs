using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Chrome;

namespace AmazonOnlineStores.BaseClass
{
   
    public class BaseTest
    {
        public static IWebDriver driver;
        public string homeURL;
        [SetUp]
        public void Open()
        {
            homeURL = "https://www.amazon.in/";
            driver = new ChromeDriver(@"C:\Users\RAJESHVARRA\source\repos\AmazonOnlineStores\Drivers\");
            driver.Navigate().GoToUrl(homeURL);
            driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void Close()
        {
            driver.Quit();
        }
    }
}
