using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmazonOnlineStores.PageObjects
{
  public  class AddToCartPage
    {
        IWebDriver driver;
        public AddToCartPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);

        }

       

        [FindsBy(How = How.XPath, Using = "//*[@id='sc-active-cart']/div/div/div[2]/div[1]/h2")]
        public IWebElement CartEmptyelement { get; set; }

        
        public string ValideEmptyCart()
        {
            return CartEmptyelement.Text;
        }
       
    }
}
