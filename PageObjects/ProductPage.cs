using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace AmazonOnlineStores.PageObjects
{
    public class ProductPage
    {
        IWebDriver driver;
        public ProductPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);

        }
        [FindsBy(How = How.XPath, Using = "//*[@id='productTitle']")]
        public IWebElement productTitleElement { get; set; }

        [FindsBy(How = How.Id, Using = "add-to-cart-button")]
        public IWebElement addToCart { get; set; }

        public string NavigateToProductscreen(string productName)
        {
            driver.FindElement(By.LinkText(productName)).Click();
            Thread.Sleep(5000);
            //Navigate to the product Details Window
            IList<string> tabs = new List<string>(driver.WindowHandles);
            driver.SwitchTo().Window(tabs[1]);
            return productTitleElement.Text;
        }
        public string AddToCart(string productName)
        {

            //Click on  Add to Cart 
            addToCart.Click();
            //add the product to cart successfully
            Thread.Sleep(5000);
            //Navigate to the product Details Window
            IList<string> tabs = new List<string>(driver.WindowHandles);
            driver.SwitchTo().Window(tabs[1]);
            IWebElement slideshettcart = driver.FindElement(By.CssSelector("div[class='a-section attach-desktop-sideSheet']"));
            IWebElement addCartElement = driver.FindElement(By.XPath("//*[@id='attachDisplayAddBaseAlert']/div/h4"));
            string addedsucess = addCartElement.Text;
            IWebElement closeSlideCart = driver.FindElement(By.XPath("//*[@id='attach-close_sideSheet-link']"));
            closeSlideCart.Click();
            return addedsucess;
        }

    }
}




       
   

