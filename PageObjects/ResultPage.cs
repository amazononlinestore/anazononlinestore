using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Collections.ObjectModel;
using OpenQA.Selenium.Support.UI;

namespace AmazonOnlineStores.PageObjects
{
   public  class ResultPage
    {
        IWebDriver driver;
        public ResultPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
           
        }

        [FindsBy(How = How.XPath, Using = "//*[@id='search']/span/div/span/h1/div/div[1]/div/div/span[3]")]
        public IWebElement SearchedText { get; set; }

        
        public string ValidateSearch()
        {
            return SearchedText.Text;
        }
        public string IsProductAvaialble(string productName)
        {
            Thread.Sleep(5000);
            IList<IWebElement> listofProducts = driver.FindElements(By.XPath("//div[contains(@class, 'sg-col-inner')]/div/h2/a/span"));
            string productName1 = productName;
            string isAvaiable= null;
         
            for (int i=0;i< listofProducts.Count; i++)
            
              {
                IWebElement temp = listofProducts[i];
                string productNameWebelement = temp.Text;
                if (String.Equals(productNameWebelement, productName1))
                  {
                      isAvaiable = "Avaiable";
                    break;
                  }
                  else 
                  {
                      isAvaiable = "Not Avaiable";
                   }

              }
            Thread.Sleep(1000);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0, document.body.scrollHeight - 150)");
            Thread.Sleep(5000);
            return isAvaiable;
        }

    }
}
