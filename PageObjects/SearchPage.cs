using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;

namespace AmazonOnlineStores.PageObjects
{
   public class SearchPage
    {
        IWebDriver driver;
        public SearchPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
            ;
        }

        [FindsBy(How = How.Id, Using = "twotabsearchtextbox")]
        public IWebElement searchProductName { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='nav-search-submit-button']")]
        public IWebElement searchElement { get; set; }
      
                      
        public void searchWithProductName(string productName)
        {
            searchProductName.SendKeys(productName);
            Thread.Sleep(5000);
            searchElement.Click();
            Thread.Sleep(5000);
        }

       
    }
}
